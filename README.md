# vue_shop

### 前言
该项目是基于vue2以及Vue-cli3脚手架搭建的PC端前端项目，其技术栈有：**Vue-Router+Vue+Axios+Element UI+Echarts**。该项目是在我学HTML5+CSS3+JavaScript以及Vue知识后搭建完成的项目，这旨在复习巩固之前所学知识，同时也尝试独立完成项目。虽然这个项目还有一些不足，但是，对我来说也算开了一个好头，对前端项目的搭建流程有了一定的了解。下面是对项目的介绍。

### 项目开发模式

前后端分离，后端写接口，前端实现接口调用、并且将数据渲染到页面上。

### 项目流程
 - 通过 cookie 在客户端记录状态
 -   通过 session 在服务器端记录状态
 -   通过 token 方式维持状态
 -   前端与服务器之间存在跨域问题时使用
 -   登录前输入帐号、密码
--> 服务器验证后返回token 
 --> 客户端保存token 
 --> 后续请求都携带token数据发送请求（用户的验证） 
 -->  服务器验证token是否通过

### 项目准备工作

 - 首先，需要下载vscode，phpstudy_pro软件。
 - 初始化GIt远程仓库，将本地项目托管到GItee中。
 - 项目开始前先创建master主分支。
 - 创建一个Login组件(三标签) :  `template`(一: 模板)  `script`(二: 属性/方法)  `<style lang="less" scoped>`  (三: 样式 支持less并且具有作用域)
 - 创建路由组件: 并且使用重定向  `redirect`  如果用户访问的是根路由路径 会自动重定向到登录的路由组件

### Login组件
1.  背景居中 使用element-ui 设置头像框 使用  `position: abolut`  和  `transform: translate(-50%, -50%);`  样式居中
2.  Element-ui 表单按钮的使用 因为设置了按需加载需要在  `plugins`文件夹下的`element.js`  配置引入的  `{Button, Form, FormItem, Input}`

-   `<el-form label-width="80px">`(表单默认label属性占位80px)  `<el-form-item label="用户名">`(一个项使用label属性左侧文字的显示)
    -   使用第三方的字体图标: 引入第三方的库 再到标签上使用类名
    -   将属性数据绑定到表单  `<el-form :rules="form" :model="loginForm">`  form的对象中  `loginForm`的数据保存在data中
    -   在表单项中再通过v-model来访问表单对象的属性`<el-input v-model="loginForm.username"></el-input>`
    -   表单的数据验证: 给表单`<el-form>`绑定`rules`属性校验(存放在`data`) 例子:  `rules: { name: [{ required: true, message: '请输入活动名称', trigger: 'blur' }] }`
        -   `required`  必填不能为空  `message`  错误消息提示  `trigger`触发事件  `min/max`  最小/大输入长度
        -   通过`prop`属性`rules`调用验证: 给  `el-form-item`  添加而不是里面的标签  `el-input`:  `<el-form-item prop="username">`
        -   表单的重置: 给`<el-form ref='loginForm'>`  绑定一个ref 通过$refs拿到组件对象,通过点击事件使用表单上自带的resetFields()重置方法
    

### Home组件
-   表单的预登录:  `validate`  方法 对象rules的规则进行校验也是用过$refs拿到组件调用其身上的方法
    
    -   验证通过后发送axios(Ajax)请求 把axios引入并赋值给Vue的原型上(可以全局访问) :`Vue.prototype.$http = axios`
-   消息提示: 添加 message 全局(Vue原型)添加`$message = Message`  error:失败的提示 success:成功的提示
    
-   登录成功之后的 token,保存到客户端的 sessionStorage(会话机制/只在当前页面生效)中 / localStorage(持久话机制/关闭页面也不会忘记数据)
    
-   `window.sessionStorage.setItem('token', res.data.token)`  因为除了登录界面都需要token来验证操作所以登录时会保存token作为身份验证
    
-   保存token后自动跳转到后台界面 使用`this.$router.push('/home')`
    
-   防止别人直接访问`login`以外的路由,而需要调用 路由的 导航守卫 使用  `beforeEach`  离开之前 检查是否有token的存在如果没有就直接跳转到 login 页面
    
-   `router.beforeEach((to, from, next) => {})`  to 即将跳转到哪里(到那里去) from 在哪里跳转(从哪里来) next 放行枷(给不给走)
    
-   退出登录: 在home页面设置按钮 清空token并且跳转到 login

### Element-ui


-   element-ui 提供的组件,每个组件名都是它自己的类名
    
-   布局容器: 
    
    -   右侧菜单(二级可折叠)  `el-menu`(最外层包裹菜单)  `<el-submenu>`一级菜单  `<el-menu-item>`  二级菜单(里层)  `<template>`  菜单的模板(icon/span)
-   请求拦截器: 登录授权 请求验证是否有 token 需要授权的 API ，必须在请求头中使用  `Authorization`  字段提供  `token`  令牌
    
    -   login 不需要 token 可以直接登录 登录进去后每次操作/请求都会验证  `Authorization`  的 token令牌
-   创建完成后立即发送 网络请求 请求左侧菜单栏的数据 get
    
    -   通过 async 和 await 来获取需要的数据 因为是数组所以可以使用 v-for 来遍历生成数据 第一级的icon不同的解决方法之一:定义一个对象来存放字体图标需要的类名
    -   菜单栏只打开一个的可以给`el-menu`  添加  `unique-opened`  属性(1) 为  `true`  | 折叠属性(2):  `collapse`  | 关闭过渡动画属性(3):  `:collapse-transition="false"`  |
    -   左侧边栏的宽度变化:  `:width="isCollapse ? '61px' : '200px'"`  利用三元表达式
    -   子菜单的跳转:  `el-from`  有router(index属性)默认为false关闭的 index='/login' index做路由跳转
        -   里面的组件都是作为Home的子组件展示的,如果作为一个独立的路由而不是Home的子路由那么左侧的导航栏就销毁没有了
    -   左侧导航激活的高亮`:default-active="activePath"`: 点击导航-> 使用sessionStorage来保存激活的路径 并赋值给高亮的变量-> 当离开再回来created时得到 sessionStorage 的路径 赋值给 高亮变量 (导航守卫.beforeEach)
  
   

### 用户管理

-   导航的名称:  `el-breadcrumb`  面包屑 : 首页 > 用户管理 > 用户列表
-   卡片搜索框的使用:  `el-card`  配合 栅栏布局 使用 input复合框 : 样式配合 Row 和 Col的栅栏配合
-   使用get获取用户数据 参数为 params { params : {name:'LHJ'} }
-   表格数据:  `<el-table :data="数据源" stripe(avtice) border(边框)>`  `<el-table-column prop="数据名" label="列的名字">`
    -   显示按钮使用作用预插槽: 在`<el-table-column>`  添加template模板再使用`v-slot`属性拿到当前槽作用域的布尔值 Boolean 再通过Switch组件显示 而在  `<el-table-column>`  使用了作用域插槽会覆盖当前层的prop所以可以删除prop 按钮使用时需要 插槽作用域
-   分页:  `pagination`:  `page-sizes`  每页显示个数选择器的选项设置  `page-size`  每页显示条目个数，支持 .sync 修饰符 number  `layout`: 显示那些组件 监听改变事件 页码的修改 显示个数的修改  `handleSizeChange(newValue)`  监听显示页数的改变自带参数 是 新的值  `handleCurrentChange`监听页码的改变
-   按钮状态的修改: 通过Switch的chang改变事件触发回调函数
-   necktick
-   搜索功能: 给搜索框双向绑定到  `queryInfo.query`  因为搜索时根据它来的 再搜索按钮绑定点击事件发送用户数据请求,根据query返回对应的参数 , 清空搜索框并清空搜索的内容 element-ui的搜索框有自带的clear事件,点击清楚时再次发送用户数据请求,此时因为query已经清空所以返回的是默认的数据
-   点击添加用户弹出  `:visible.sync = DialogVisble` 为true显示反之隐藏
-   添加  **el-form**  项 :model="绑定要显示数据的对象" :rules="绑定校验规则的对象" ref="重置表单数据素要的方法"
    -   手机和邮箱验证规则: 在data里面与return同级 用变量接收一个箭头函数(ruel, value, callback) 里面时校验的正则表达式 通过直接callback() 不通过则callback(new Error(''))
    -   使用变量名的方法:  `{ validator: checkEmail, trigger: 'blur' }`  在验证规则里面写下`validator: 变量名`  就可以调用正则表达式来验证邮箱或手机号码
-   调用form的validate属性判断数据是否合法, 值是true就发送网络请求添加用户 否则直接返回结束函数 post 对象 if(res.meta.status) 201为创建成功
    -   对话框的代码可以放到外面,只需要使用点击事件来变换 布尔值 就可以做到显示和隐藏
-   修改用户信息: 点击按钮使用作用插槽传id值,再发送axios.get请求获取当前id的用户 并将数据保存到起来
    -   点击确认按钮: 表单预登录验证 如果成功就发送 put请求并将保存的数据作为参数修改(因为双向绑定)
-   删除用户: 点击删除按钮 弹出对话框 -> 确认是否删除; 需要使用ui的 MessageBox 且要全局挂载在Vue.prototype.confirm = MessageBox .confirm

### 商品分类

-   树形表格:由于 element-ui 没有 树形的表格要借助于第三方的  `vue-table-with-tree-grid`  
    
    -   columns｛ｌａｂｅｌ template type prop｝ -- show-index index-text　下面案例
-   分页: 由于获取用户的方法关系,可以每次修改 页码 或 页数 时直接重新发送获取用户请求

### 项目优化
 -   生成打包报告
 - 项目优化 配置webpack
 - 通过 externals 加载外部 CDN 资源
 - 路由懒加载
 - 开启gzip包 compression
 - pm2 关闭命令行窗口依旧执行
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE0OTE2NjgyNzksLTEwNDU3ODczMzksMT
c3MjI3MDQ5OCw0NzEwMDUyNDYsODM5Mzk3MzcsMjMyMDEzNTYs
NDI1NTU1NTY3LDg3OTgxNzE2LC04MjMxNjI1MjIsLTE5MTU4MT
k3MjBdfQ==
-->